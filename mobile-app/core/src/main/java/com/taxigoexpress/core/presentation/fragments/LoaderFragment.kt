package com.taxigoexpress.core.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager

import com.taxigoexpress.core.R
import com.taxigoexpress.core.presentation.utils.Constants
import kotlinx.android.synthetic.main.fragment_loader_layout.*

/**
 * A simple [Fragment] subclass.
 */
class LoaderFragment : BaseDialogFragment() {

    private var isShowing = false
    private val loadingMessage: String
        get() = arguments?.getString(Constants.LOADING_MESSAGE, "") ?: getString(R.string.loading)


    companion object {
        fun newInstance(loadingMessage: String? = ""): LoaderFragment {
            val loadingDialog = LoaderFragment()
            /*val bundle = Bundle()
            bundle.putString(Constants.LOADING_MESSAGE, loadingMessage)
            loadingDialog.arguments = bundle*/
            return loadingDialog
        }
    }

    init {
        isCancelable = false
    }

    override fun getStyle(): Int? = R.style.DialogTransparent

    override fun getLayout(): Int = R.layout.fragment_loader_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.dialog_loading_animation.playAnimation()
        this.loading_message.text = this.loadingMessage
    }

    override fun show(manager: FragmentManager, tag: String?) {
        if (!isShowing) {
            isShowing = true
            try {
                val fragmentTransaction = manager?.beginTransaction()
                fragmentTransaction.add(this, tag)
                fragmentTransaction.commitAllowingStateLoss()
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
        }
    }


    override fun dismiss() {
        isShowing = false
        super.dismissAllowingStateLoss()
    }


}
