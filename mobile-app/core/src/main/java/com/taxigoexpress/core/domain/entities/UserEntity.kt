package com.taxigoexpress.core.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UserEntity(

    @field:SerializedName("id")
    private val _id: Int? = null,

    @field:SerializedName("nombre")
    private val _name: String? = null,

    @field:SerializedName("apellido")
    private val _lastName: String? = null,

    @field:SerializedName("telefono")
    private val _phoneNumber: String? = null,

    @field:SerializedName("correo")
    private val _email: String? = null,

    @field:SerializedName("password")
    private val _password: String? = null,

    @field:SerializedName("rating")
    private val _rating: Double? = null,

    @field:SerializedName("fechaCreacion")
    private val _creationDate: String? = null,

    @field:SerializedName("permiso")
    private val _permission: String? = null,

    @field:SerializedName("numeroLicencia")
    private val _licenseNumber: String? = null,

    var isAcceptedTC: Boolean = false

) : BaseResponse(), Serializable {
    val id: Int
        get() = _id ?: 0

    val name: String
        get() = _name ?: ""

    val lastName: String
        get() = _lastName ?: ""

    val phoneNumber: String
        get() = _phoneNumber ?: ""

    val email: String
        get() = _email ?: ""

    val permission: String
        get() = _permission ?: ""

    val licenseNumber: String
        get() = _licenseNumber ?: ""

    val password: String
        get() = _password ?: ""

    val rating: Double
        get() = _rating ?: 0.0

    val createdDate: String
        get() = _creationDate ?: ""

}
