package com.taxigoexpress.core.domain.executors

import io.reactivex.Scheduler

interface PostExecutionThread {

    fun getScheduler() : Scheduler
}