package com.taxigoexpress.core.presentation.managers

import com.facebook.accountkit.Account
import com.facebook.accountkit.AccountKitError

interface AccountKitCallback {
    fun onAuthenticationSuccess(account: Account)
    fun onAuthenticationError(accountKitError: AccountKitError)
}