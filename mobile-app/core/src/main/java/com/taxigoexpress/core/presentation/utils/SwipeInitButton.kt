package com.taxigoexpress.core.presentation.utils

import android.animation.*
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.taxigoexpress.core.R


class SwipeInitButton : RelativeLayout {

    private lateinit var slidingButton: ImageView
    private var initialX = 0f
    private var active = false
    private var initialButtonWidth = 0
    private lateinit var centerText: TextView

    private lateinit var disabledDrawable: Drawable
    private lateinit var enabledDrawable: Drawable

    constructor(context: Context) : super(context) {
        init(context, null, -1, -1)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs, -1, -1)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs) {
        init(context, attrs, defStyleAttr, -1)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(context, attrs, defStyleAttr, defStyleRes)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun init(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) {
        val background = RelativeLayout(context)

        val layoutParamsView = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParamsView.addRule(CENTER_IN_PARENT, TRUE)

        background.background = ContextCompat.getDrawable(context, R.drawable.shape_rounded)
        background.setPadding(40, 40, 40, 40)
        addView(background, layoutParamsView)

        val centerText = TextView(context)
        this.centerText = centerText

        val layoutParams = LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        layoutParams.addRule(CENTER_IN_PARENT, TRUE)
        centerText.text =
            this.context.getString(R.string.slide_view_init_message)//add any text you need
        centerText.setTextColor(Color.BLACK)
        background.addView(centerText, layoutParams)

        val swipeButton = ImageView(context)
        slidingButton = swipeButton

        disabledDrawable =
            ContextCompat.getDrawable(getContext(), R.drawable.ic_car_activate_service)!!
        enabledDrawable =
            ContextCompat.getDrawable(getContext(), R.drawable.ic_car_activate_service)!!

        slidingButton.setImageDrawable(disabledDrawable)
        slidingButton.setPadding(20, 20, 20, 20)

        val layoutParamsButton = LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        layoutParamsButton.addRule(ALIGN_PARENT_LEFT, TRUE)
        layoutParamsButton.addRule(CENTER_VERTICAL, TRUE)
        layoutParamsButton.marginStart = 20
        swipeButton.background = ContextCompat.getDrawable(context, R.drawable.shape_rounded)
        swipeButton.setImageDrawable(disabledDrawable)
        addView(swipeButton, layoutParamsButton)

        this.setOnTouchListener(getButtonTouchListener());

    }

    private fun getButtonTouchListener(): OnTouchListener? {
        return OnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> return@OnTouchListener true
                MotionEvent.ACTION_MOVE -> { //Movement logic here
                    if (initialX == 0f) {
                        initialX = slidingButton.x
                    }
                    if (event.x > initialX + slidingButton.width / 2 &&
                        event.x + slidingButton.width / 2 < width
                    ) {
                        slidingButton.x = event.x - slidingButton.width / 2
                        centerText.alpha =
                            1 - 1.3f * (slidingButton.x + slidingButton.width) / width
                    }

                    if (event.x + slidingButton.width / 2 > width &&
                        slidingButton.x + slidingButton.width / 2 < width
                    ) {
                        slidingButton.x = (width - slidingButton.width).toFloat()
                    }

                    if (event.x < slidingButton.width / 2 &&
                        slidingButton.x > 0
                    ) {
                        slidingButton.x = 0f
                    }
                    return@OnTouchListener true
                }
                MotionEvent.ACTION_UP -> { //Release logic here
                    if (active) {
                        collapseButton()
                    } else {
                        initialButtonWidth = slidingButton.width

                        if (slidingButton.x + slidingButton.width > width * 0.85) {
                            expandButton()
                        } else {
                            moveButtonBack()
                        }
                    }
                    return@OnTouchListener true
                }
            }
            false
        }
    }

    private fun expandButton() {
        val positionAnimator = ValueAnimator.ofFloat(slidingButton.x, 0f)
        positionAnimator.addUpdateListener {
            val x = positionAnimator.animatedValue as Float
            slidingButton.x = x
        }
        val widthAnimator = ValueAnimator.ofInt(
            slidingButton.width,
            width
        )
        widthAnimator.addUpdateListener {
            val params = slidingButton.layoutParams
            params.width = (widthAnimator.animatedValue as Int)
            slidingButton.layoutParams = params
        }
        val animatorSet = AnimatorSet()
        animatorSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                active = true
                slidingButton.setImageDrawable(enabledDrawable)
            }
        })
        animatorSet.playTogether(positionAnimator, widthAnimator)
        animatorSet.start()
    }

    private fun collapseButton() {
        val widthAnimator = ValueAnimator.ofInt(
            slidingButton.width,
            initialButtonWidth
        )
        widthAnimator.addUpdateListener {
            val params = slidingButton.layoutParams
            params.width = (widthAnimator.animatedValue as Int)
            slidingButton.layoutParams = params
        }
        widthAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                active = false
                slidingButton.setImageDrawable(disabledDrawable)
            }
        })
        val objectAnimator = ObjectAnimator.ofFloat(
            centerText, "alpha", 1f
        )
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(objectAnimator, widthAnimator)
        animatorSet.start()
    }

    private fun moveButtonBack() {
        val positionAnimator = ValueAnimator.ofFloat(slidingButton.x, 0f)
        positionAnimator.interpolator = AccelerateDecelerateInterpolator()
        positionAnimator.addUpdateListener {
            val x = positionAnimator.animatedValue as Float
            slidingButton.x = x
        }
        val objectAnimator = ObjectAnimator.ofFloat(
            centerText, "alpha", 1f
        )
        positionAnimator.duration = 200
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(objectAnimator, positionAnimator)
        animatorSet.start()
    }
}