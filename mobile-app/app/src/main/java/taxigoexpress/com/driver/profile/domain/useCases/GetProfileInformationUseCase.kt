package taxigoexpress.com.driver.profile.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import taxigoexpress.com.driver.profile.domain.repositoryAbstractions.ProfileRepository
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import javax.inject.Inject

class GetProfileInformationUseCase
@Inject constructor(
    private val profileRepository: ProfileRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<UserEntity, String>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: String): Observable<UserEntity> =
        this.profileRepository.getProfileInformation(profileId = params)
}