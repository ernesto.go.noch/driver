package taxigoexpress.com.driver.profile.presentation.components

import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import taxigoexpress.com.driver.profile.presentation.modules.ProfileModule
import dagger.Component
import taxigoexpress.com.driver.profile.presentation.fragments.ProfileFragment

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ProfileModule::class]
)
interface ProfileComponent {
    fun inject(profileFragment: ProfileFragment)
}