package taxigoexpress.com.driver.trips.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import taxigoexpress.com.driver.HomeActivity

import taxigoexpress.com.driver.R

/**
 * A simple [Fragment] subclass.
 */
class MainTripViewFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_main_trip_view_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        (this.activity as HomeActivity).showMainToolbar()
    }

    companion object {
        fun newInstance(): MainTripViewFragment {
            return MainTripViewFragment()
        }
    }
}
