package taxigoexpress.com.driver.profile.presentation.viewmodels.implementations

import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import taxigoexpress.com.driver.profile.domain.useCases.GetProfileInformationUseCase
import taxigoexpress.com.driver.profile.presentation.observers.ProfileObserver
import taxigoexpress.com.driver.profile.presentation.viewmodels.abstractions.ProfileViewModel
import com.taxigoexpress.core.domain.entities.UserEntity
import javax.inject.Inject

class ProfileViewModelImpl
@Inject constructor(private val getProfileInformationUseCase: GetProfileInformationUseCase) :
    BaseViewModelLiveData<UserEntity>(), ProfileViewModel<UserEntity> {

    private val mProfileLiveData by lazy { this.getCustomLiveData() }

    override fun getProfileInformation(profileId:String) {
        this.getProfileInformationUseCase.execute(
            ProfileObserver(mProfileLiveData),
            profileId
        )
    }

    override fun observeProfileInformation(): BaseLiveData<UserEntity> =
        this.mProfileLiveData

    override fun observeResult(): BaseLiveData<UserEntity> =
        this.mProfileLiveData

    override fun onCleared() {
        this.getProfileInformationUseCase.dispose()
        super.onCleared()
    }
}