package taxigoexpress.com.driver.profile.presentation.modules

import com.taxigoexpress.core.presentation.scopes.FragmentScope
import taxigoexpress.com.driver.profile.data.repositoryImplementations.ProfileRepositoryImpl
import taxigoexpress.com.driver.profile.data.services.ProfileServices
import taxigoexpress.com.driver.profile.domain.repositoryAbstractions.ProfileRepository
import taxigoexpress.com.driver.profile.presentation.viewmodels.abstractions.ProfileViewModel
import taxigoexpress.com.driver.profile.presentation.viewmodels.implementations.ProfileViewModelImpl
import com.taxigoexpress.core.domain.entities.UserEntity
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class ProfileModule {

    @Provides
    @FragmentScope
    fun providesProfileServices(@Named("main_retrofit") retrofit: Retrofit): ProfileServices =
        retrofit.create(ProfileServices::class.java)

    @Provides
    @FragmentScope
    fun providesProfileRepository(profileRepositoryImpl: ProfileRepositoryImpl): ProfileRepository =
        profileRepositoryImpl

    @Provides
    @FragmentScope
    fun providesProfileViewModel(profileViewModelImpl: ProfileViewModelImpl): ProfileViewModel<UserEntity> =
        profileViewModelImpl
}