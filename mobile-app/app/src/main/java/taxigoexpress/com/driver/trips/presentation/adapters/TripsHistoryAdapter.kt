package taxigoexpress.com.driver.trips.presentation.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ia.mchaveza.kotlin_library.inflate
import taxigoexpress.com.driver.R

class TripsHistoryAdapter:RecyclerView.Adapter<TripsHistoryAdapter.Viewholer>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholer  =
        Viewholer(parent.inflate(R.layout.item_trips_history))

    override fun getItemCount(): Int =  8

    override fun onBindViewHolder(holder: Viewholer, position: Int) {
    }

    class Viewholer(itemView: View):RecyclerView.ViewHolder(itemView) {

    }

}
