package taxigoexpress.com.driver.statistics.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import taxigoexpress.com.driver.statistics.presentation.fragments.PeriodStadisticsFragment

class StadisticsViewPagerAdapter(
    fm
    : FragmentManager
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val monthFragment = PeriodStadisticsFragment.newInstance()
    private val dayFragment = PeriodStadisticsFragment.newInstance()

    override fun getItem(position: Int): Fragment =
        if (position == 0) {
            monthFragment
        } else {
            dayFragment
        }


    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? =
        if (position == 0) {
            "Día"
        } else {
            "Semana"
        }

}
