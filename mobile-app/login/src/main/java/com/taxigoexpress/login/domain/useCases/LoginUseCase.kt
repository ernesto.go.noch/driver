package com.taxigoexpress.login.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.core.presentation.utils.isValidEmail
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.domain.exceptions.LoginException
import com.taxigoexpress.login.domain.repositoryAbstractions.LoginRepository
import io.reactivex.Observable
import javax.inject.Inject

class LoginUseCase
@Inject constructor(
    private val loginRepository: LoginRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<Unit, LoginRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: LoginRequest): Observable<Unit> = when {
        params._password.isEmpty() -> Observable.error(LoginException(LoginException.Type.PASSWORD_EMPTY_ERROR))
        !params._email.isValidEmail() -> Observable.error(LoginException(LoginException.Type.EMAIL_WRONG_FORMAT))
        else -> this.loginRepository.login(params)
    }
}