package com.taxigoexpress.login.presentation.viewmodels.abstractions

import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.presentation.liveData.LoginLiveData

interface LoginViewModel<T> : PresenterLiveData<T> {
    fun login(request: LoginRequest)
    fun observeLogin(): LoginLiveData
}