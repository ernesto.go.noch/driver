package com.taxigoexpress.login.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginServices {

    @POST(BuildConfig.LOGIN)
    fun login(
        @Body loginRequest: LoginRequest
    ):Observable<Response<Unit>>
}