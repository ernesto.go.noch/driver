package com.taxigoexpress.login.data.repositoryImplementations

import com.taxigoexpress.login.data.datasourceImplementations.LoginDataSourceImpl
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.domain.repositoryAbstractions.LoginRepository
import io.reactivex.Observable
import javax.inject.Inject

class LoginRepositoryImpl
@Inject constructor(private val signinDatasourceImpl: LoginDataSourceImpl): LoginRepository {

    override fun login(params: LoginRequest): Observable<Unit> =
        this.signinDatasourceImpl.login(params)

}