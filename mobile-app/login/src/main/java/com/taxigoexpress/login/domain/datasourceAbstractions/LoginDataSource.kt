package com.taxigoexpress.login.domain.datasourceAbstractions

import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import io.reactivex.Observable

interface LoginDataSource {
    fun login(params: LoginRequest): Observable<Unit>

}
