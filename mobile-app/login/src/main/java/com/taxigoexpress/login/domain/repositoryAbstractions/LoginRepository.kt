package com.taxigoexpress.login.domain.repositoryAbstractions

import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import io.reactivex.Observable


interface LoginRepository {
    fun login(params: LoginRequest): Observable<Unit>

}
