package com.taxigoexpress.login.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.login.R
import com.taxigoexpress.login.presentation.activities.LoginActivity
import com.taxigoexpress.login.presentation.components.DaggerLoginComponent
import com.taxigoexpress.login.presentation.components.LoginComponent
import com.taxigoexpress.login.presentation.modules.LoginModule
import com.taxigoexpress.login.presentation.viewmodels.abstractions.LoginViewModel
import com.taxigoexpress.register.presentation.fragments.RegisterFragment
import kotlinx.android.synthetic.main.fragment_login_layout.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : BaseFragment() {

    @Inject
    lateinit var loginViewModel: LoginViewModel<Unit>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(com.taxigoexpress.register.R.string.loading)) }

    private val loginComponent: LoginComponent by lazy {
        DaggerLoginComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .loginModule(LoginModule())
            .build()
    }


    override fun getLayout(): Int = R.layout.fragment_login_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        loginComponent.inject(this)
        this.configureToolbar()
        this.loadView()
        this.observeResults()
    }

    private fun configureToolbar() {
        (this.activity as LoginActivity).showToolbar()
    }

    private fun loadView() {
        this.btnForgotPassword.clickEvent().observe(this, Observer {

        })

        this.btnLogin.clickEvent().observe(this, Observer {
            this.activity?.finish()
        })
    }

    private fun observeResults() {
        with(this.loginViewModel.observeLogin()) {
            this.loginObserver.customObserver.observe(this@LoginFragment, Observer {
                this@LoginFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.USER,
                    this@LoginFragment.etMail.text.toString()
                )
                this@LoginFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.PASS_ID,
                    this@LoginFragment.etPassword.text.toString()
                )
                this@LoginFragment.activity?.finish()
            })

            this.loginObserver.loadingObserver.observe(this@LoginFragment, Observer {
                if (it == View.VISIBLE) {
                    this@LoginFragment.loaderDialog.show(
                        childFragmentManager,
                        RegisterFragment::class.java.name
                    )
                } else {
                    this@LoginFragment.loaderDialog.dismiss()
                }
            })


            this.loginObserver.showErrorObserver.observe(this@LoginFragment, Observer {
                this@LoginFragment.showAlertWithResource(it ?: 0)
            })

            this.loginObserver.showExceptionObserver.observe(
                this@LoginFragment,
                Observer {
                    this@LoginFragment.showAlert("Error al inicio de sesión")
                })

            this.emailErrorObserver.observe(this@LoginFragment, Observer {
                this@LoginFragment.showAlertWithResource(it ?: 0)
            })

            this.passwordErrorObserver.observe(this@LoginFragment, Observer {
                this@LoginFragment.showAlertWithResource(it ?: 0)
            })
        }
    }

    companion object {
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }


}
