package com.taxigoexpress.register.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.Observer
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent

import com.taxigoexpress.register.R
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.presentation.components.DaggerRegisterComponent
import com.taxigoexpress.register.presentation.modules.RegisterModule
import com.taxigoexpress.register.presentation.viewmodels.abstractions.RegisterViewModel
import kotlinx.android.synthetic.main.fragment_register_layout.*
import kotlinx.android.synthetic.main.register_survey_layout.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : BaseFragment() {

    @Inject
    lateinit var registerViewModel: RegisterViewModel<UserEntity>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(R.string.loading)) }

    private val registerComponent by lazy {
        DaggerRegisterComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .registerModule(RegisterModule())
            .build()
    }

    override fun getLayout(): Int = R.layout.fragment_register_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.registerComponent.inject(this)
        this.observeResults()
        this.setClickListeners()
    }

    private fun setClickListeners() {
        this.btnRegister.clickEvent().observe(this, Observer {
            this.registerViewModel.register(
                UserEntity(
                    null,
                    this.etName.text.toString(),
                    this.etLast.text.toString(),
                    this.etSurveyPhone.text.toString(),
                    this.etSurveyEmail.text.toString(),
                    this.etSurveyPassword.text.toString(),
                    0.0,
                    "",
                    this.etSurveyPermission.text.toString(),
                    this.etSurveyLicenseNumber.text.toString(),
                    this.cbTermsConditions.isChecked
                )
            )
//            this.replaceFragment(
//                DocumentsFragment.newInstance(),
//                R.id.register_content,
//                true
//            )
        })

        this.llTermsConditions.clickEvent().observe(this, Observer {
            this.cbTermsConditions.isChecked = !this.cbTermsConditions.isChecked
        })
    }

    private fun observeResults() {

        with(this.registerViewModel.observeRegister()) {

            this.registerObserver.customObserver.observe(this@RegisterFragment, Observer {
                //                AccountKitManager.getCurrentAccount(this@RegisterFragment, this@RegisterFragment)
                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.USER,
                    it.email
                )
                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.PASS_ID,
                    it.id.toString()
                )
                this@RegisterFragment.keyStoreManager?.saveSensitiveData(
                    Constants.PASSWORD,
                    etSurveyPassword.text.toString()
                )

                this@RegisterFragment.showAlertWithAction(
                    "Registro finalizado.\nEnvia un correo a " +
                            "contacto@taxigoexpress.com" +
                            "para terminar con tu registro"
                ) {
                    activity?.finish()
                    true
                }

            })

            this.registerObserver.loadingObserver.observe(this@RegisterFragment, Observer {
                if (it == View.VISIBLE) {
                    this@RegisterFragment.loaderDialog.show(
                        childFragmentManager,
                        RegisterFragment::class.java.name
                    )
                } else {
                    this@RegisterFragment.loaderDialog.dismiss()
                }
            })

            this.nameErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.lastNameErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.passwordErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.emailErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.phoneNumberErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.uncheckTCObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.registerObserver.showErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.registerObserver.showExceptionObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlert(it)
            })

        }
    }

    companion object {
        fun newInstance(): RegisterFragment {
            return RegisterFragment()
        }
    }


}
