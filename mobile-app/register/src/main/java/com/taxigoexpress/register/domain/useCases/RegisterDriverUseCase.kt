package com.taxigoexpress.register.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.core.presentation.utils.isValidEmail
import com.taxigoexpress.core.presentation.utils.isValidPassword
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.domain.exceptions.RegisterException
import com.taxigoexpress.register.domain.repositoryAbstractions.RegisterRepository
import io.reactivex.Observable
import javax.inject.Inject

class RegisterDriverUseCase
@Inject constructor(
    private val registerRepository: RegisterRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<UserEntity, UserEntity>(threadExecutor, postExecutionThread) {


    override fun createObservable(params: UserEntity): Observable<UserEntity> = when {
        params.name.length <= 2 -> Observable.error(RegisterException(RegisterException.Type.NO_VALID_NAME))
        params.lastName.length <= 2 -> Observable.error(RegisterException(RegisterException.Type.NO_VALID_LASTNAME))
        !params.password.isValidPassword() -> Observable.error(RegisterException(RegisterException.Type.INVALID_PASSWORD))
        !params.email.isValidEmail() -> Observable.error(RegisterException(RegisterException.Type.NO_VALID_EMAIL))
        params.phoneNumber.length < 10 -> Observable.error(RegisterException(RegisterException.Type.NO_VALID_PHONE_NUMBER))
        !params.isAcceptedTC -> Observable.error(RegisterException(RegisterException.Type.UNCHECK_TC))
        else -> this.registerRepository.registerUser(params).flatMap {
            this.registerRepository.registerDriver(
                this.setDataToDriver(
                    it,
                    params.password,
                    params.licenseNumber,
                    params.permission
                )
            )
        }
    }

    private fun setDataToDriver(
        it: UserEntity,
        password: String,
        licenseNumber: String,
        permission: String
    ): UserEntity {
        return UserEntity(
            it.id,
            it.name,
            it.lastName,
            it.phoneNumber,
            it.email,
            password,
            5.0,
            it.createdDate,
            permission,
            licenseNumber
        )
    }

}