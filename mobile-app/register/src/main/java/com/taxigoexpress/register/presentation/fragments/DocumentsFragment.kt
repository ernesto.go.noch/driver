package com.taxigoexpress.register.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.register.R

/**
 * A simple [Fragment] subclass.
 */
class DocumentsFragment : BaseFragment() {

    override fun getLayout(): Int  = R.layout.fragment_documents_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
    }

    companion object{
        fun newInstance():DocumentsFragment{
            return DocumentsFragment()
        }
    }

}
