package com.taxigoexpress.register.data.datasourceImplementations

import com.taxigoexpress.register.data.services.RegisterServices
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.domain.datasourceAbstractions.RegisterDataSource
import io.reactivex.Observable
import javax.inject.Inject

class RegisterDataSourceImpl
@Inject constructor(private val service: RegisterServices) : RegisterDataSource {

    override fun registerUser(request: UserEntity): Observable<UserEntity> =
        this.service.register(request).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }

    override fun registerDriver(userId: String, request: UserEntity): Observable<UserEntity> =
        this.service.registerDriver(userId, request).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }


}
