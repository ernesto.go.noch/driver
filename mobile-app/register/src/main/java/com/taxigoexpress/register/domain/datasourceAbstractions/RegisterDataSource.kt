package com.taxigoexpress.register.domain.datasourceAbstractions

import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable

interface RegisterDataSource {
    fun registerUser(request: UserEntity): Observable<UserEntity>
    fun registerDriver(userId: String, request: UserEntity): Observable<UserEntity>

}
